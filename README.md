# README
# CADTKD

Simple solution to maintain records of athletes, their masters and places where they practice Taekwondo.

## Getting Started

1. git clone https://github.com/caws/cadtkd
2. bundle
3. rails s

Using Docker:

1. Install Docker and DockerCompose. 
2. $docker-compose build
3. $docker-compose up -d
 

### Prerequisites

- Ruby V2.3.3p222
- Rails V5.1.5
- Postgres
- Docker
- Bootstrap v4.0.0-beta.2


### Installing

As of now you have to have your postgres user set as "postgres" and their password as "postgres" too.

## Built With

* [Ruby on Rails](http://rubyonrails.org/) - Framework
* [SB Admin](https://startbootstrap.com/template-overviews/sb-admin/) - Base theme
* [Clean Blog](https://startbootstrap.com/template-overviews/clean-blog/) - External pages

## Authors

* **Adriano Moreira Lima** - https://github.com/AdrianoMorsa
* **Charles Washington de Aquino dos Santos** - https://github.com/caws
* **Efraim Gentil** - https://github.com/efraimgentil
* **Rafael Paz** - https://github.com/rafa160
* **Rafael Banhos Sales** - https://github.com/rafaelbanhos