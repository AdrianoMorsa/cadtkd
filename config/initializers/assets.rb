# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in the app/assets
# folder are already added.
 #Clean Blog
 Rails.application.config.assets.precompile += %w( clean-blog.css ) 
 Rails.application.config.assets.precompile += %w( clean-blog/about.css clean-blog/about.js )
 Rails.application.config.assets.precompile += %w( clean-blog/contact.css clean-blog/contact.js )
 Rails.application.config.assets.precompile += %w( clean-blog/index.css clean-blog/index.js )
 Rails.application.config.assets.precompile += %w( clean-blog/post.css clean-blog/post.js )

 #SB Admin
 Rails.application.config.assets.precompile += %w( sb-admin.css )
 Rails.application.config.assets.precompile += %w( sb-admin.js )
 Rails.application.config.assets.precompile += %w( sb-admin/bootstrap.min.css )
 Rails.application.config.assets.precompile += %w( sb-admin/font-awesome.min.css )
 Rails.application.config.assets.precompile += %w( sb-admin/sb-admin.css )
 Rails.application.config.assets.precompile += %w( sb-admin/jquery.min.js )
 Rails.application.config.assets.precompile += %w( sb-admin/bootstrap.bundle.min.js )
 Rails.application.config.assets.precompile += %w( sb-admin/jquery.easing.min.js )
 Rails.application.config.assets.precompile += %w( sb-admin/sb-admin.min.js )
 Rails.application.config.assets.precompile += %w( sb-admin/vendor/chart.js/chart.js )
 Rails.application.config.assets.precompile += %w( sb-admin/sb-admin-charts.min.js )
 Rails.application.config.assets.precompile += %w( sb-admin/dataTables.bootstrap4.css )
 Rails.application.config.assets.precompile += %w( sb-admin/jquery.dataTables.js )
 Rails.application.config.assets.precompile += %w( sb-admin/dataTables.bootstrap4.js )
 Rails.application.config.assets.precompile += %w( sb-admin/sb-admin-datatables.min.js )
