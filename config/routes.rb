Rails.application.routes.draw do
  
  #Setting the root

  root 'home#index'

  #sessions_controller.rb

  get    '/login',       to: 'sessions#new',        as: :session_new
  post   '/login',       to: 'sessions#create',     as: :session_create
  get    '/logout',      to: 'sessions#destroy',    as: :session_destroy

  #dashboard_controller.rb

  get    '/dashboard',   to: 'dashboard#index',     as: :dashboard_index

  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
