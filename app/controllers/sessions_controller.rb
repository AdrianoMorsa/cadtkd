class SessionsController < ApplicationController
  layout "session"
  skip_before_action :authorize

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to dashboard_index_url
    else
      flash.now[:notice] = "Invalid user or password."
      render 'new'
    end
  end

  def new
    if logged_in? 
      redirect_to dashboard_index_url
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
