class HomeController < ApplicationController
  skip_before_action :authorize
  #This controller uses a different layout file. 
  #This is mainly to organize the files needed for the page to be shown correctly.
  layout "home"

  def index
  end
  
end
