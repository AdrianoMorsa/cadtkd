class User < ApplicationRecord
    before_save { email.downcase! } #antes de salvar, deixa o e-mail minúsculo
    validates :name, presence: true #garante a presença do atributo name
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i #regex a ser utilizado na verificação de um e-mail válido
    validates :email, presence: true, #valida a presença do atributo email
                      format: { with: VALID_EMAIL_REGEX }, #formata de acordo com o regex acima
                      uniqueness: { case_sensitive: false } #garante a unicidade daquele e-mail no banco
    has_secure_password #encriptação de senha
   # validates :password, presence: true, length: { minimum: 6 } #valida a presença do atributo password e obriga tal atributo a ter um comprimento de 6 caracteres
end
